FROM kalilinux/kali-rolling
ADD test.sh /test.sh
RUN ["chmod", "7", "test.sh"]
CMD ["./test.sh"]